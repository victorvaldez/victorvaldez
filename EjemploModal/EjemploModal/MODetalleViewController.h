//
//  MODetalleViewController.h
//  EjemploModal
//
//  Created by Patlan on 21/03/14.
//  Copyright (c) 2014 Iniciales. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface _____MODetalleViewController : UITableViewController


@protocol AlumnoDetallesViewControllerDelegate <NSObject>

- (void) AlumnoDetallesViewControllerDelegateDidSave:(MODetalleViewController *) controller;
- (void) AlumnoDetallesViewControllerDelegateDidCancel:(MODetalleViewController *) controller;

@end

@interface MODetallesViewController : UITableViewController

@property (nonatomic, weak) id <AlumnoDetallesViewControllerDelegate> delegate;

- (IBAction)done:(id)sender;
- (IBAction)cancel:(id)sender;

@end
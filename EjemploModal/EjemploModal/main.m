//
//  main.m
//  EjemploModal
//
//  Created by Patlan on 21/03/14.
//  Copyright (c) 2014 Iniciales. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "_____MOAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([_____MOAppDelegate class]));
    }
}

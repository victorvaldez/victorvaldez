//
//  _____MOAppDelegate.h
//  EjemploModal
//
//  Created by Patlan on 21/03/14.
//  Copyright (c) 2014 Iniciales. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface _____MOAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

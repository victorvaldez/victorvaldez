//
//  NLViewController.m
//  ejemploFacebook
//
//  Created by Patlan on 03/04/14.
//  Copyright (c) 2014 organizacion. All rights reserved.
//

#import "NLViewController.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface NLViewController ()
- (IBAction)oprimir:(id)sender;

@end

@implementation NLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)oprimir:(id)sender {
    SLComposeViewController *controladorSocial;
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
        //check if FacebookAccount is linked
    {
        
        controladorSocial=[[SLComposeViewController alloc] init];
        //initiate the social controller
        controladorSocial=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        //Tell him with what social plattform to use it
        
        [controladorSocial setInitialText:@"sd"];
        [self presentViewController:controladorSocial animated:YES completion:nil];
        
    }
    [controladorSocial setCompletionHandler:^(SLComposeViewControllerResult result){
        
        NSString *output;
        switch (result){
                
            case SLComposeViewControllerResultCancelled:output=@"Cancelado";
                break;
            case SLComposeViewControllerResultDone:output=@"Trivia social posteada";
                break;
            default:break;
                
        }
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Facebook" message:output delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
     ];
    
}


@end

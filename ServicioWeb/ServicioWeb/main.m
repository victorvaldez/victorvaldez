//
//  main.m
//  ServicioWeb
//
//  Created by Patlan on 04/04/14.
//  Copyright (c) 2014 organizacion. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NLAppDelegate class]));
    }
}

//
//  NLViewController.h
//  ServicioWeb
//
//  Created by Patlan on 04/04/14.
//  Copyright (c) 2014 organizacion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLViewController : UIViewController <NSXMLParserDelegate>
@property (strong, nonatomic) NSMutableData *datosWeb;
@end

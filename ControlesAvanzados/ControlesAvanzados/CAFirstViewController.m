//
//  CAFirstViewController.m
//  ControlesAvanzados
//
//  Created by Patlan on 11/03/14.
//  Copyright (c) 2014 ITLM. All rights reserved.
//

#import "CAFirstViewController.h"

@interface CAFirstViewController ()

@end

@implementation CAFirstViewController
NSArray *unidades;
int tipoDeUnidades=0;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    unidades =[NSArray arrayWithObjects:[NSArray arrayWithObjects:@"Longitud",@"Centimetros",@"metro",@"kilometro",@"pie", nil],[NSArray arrayWithObjects:@"Area",@"hectareas",@"metros cuadrados", nil],[NSArray arrayWithObjects:@"Volumen",@"litros",@"metros cubicos", nil], nil];
	// Do any additional setup after loading the view, typically from a nib.
}
-(void)viewDidUnload{
    unidades =nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark pickerView
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2; //indicamos que hay dos columnas en el pickerView
    
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(component==0)
        return [unidades count];//regresamos cuantos tipos de unidades tenemos
    return [[unidades objectAtIndex:tipoDeUnidades]count]-1;
    //regresamos cuantas unidades de ese tipo tenemos restando el nombre del tipo
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(component==0)//si cambian de seleccion de tipo
        if (tipoDeUnidades !=row) {
            tipoDeUnidades=row;// guardamos el nuevo tipo
            [pickerView reloadComponent:1];
            //mandamos recargas las unidades del tipo
        }
}
-(NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(component == 0)
        return [[unidades objectAtIndex:row]objectAtIndex:0];
    return [[unidades objectAtIndex:tipoDeUnidades]objectAtIndex:row+1];
}
@end

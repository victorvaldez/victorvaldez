//
//  NLAppDelegate.h
//  NotificacionesLocales
//
//  Created by Angel Gabriel on 11/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

//
//  NLViewController.m
//  BaseDatos
//
//  Created by Patlan on 31/03/14.
//  Copyright (c) 2014 itlm. All rights reserved.
//

#import "NLViewController.h"
#import "ProxYBD.h"

@interface NLViewController ()
@property (weak, nonatomic) IBOutlet UITextField *texto;
- (IBAction)oprimir:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tabla;
@property (strong, nonatomic) ProxYBD *proxYBD;

@end

@implementation NLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self copiaBD];
    self.ProxYBD=[[ProxYBD alloc]init];
}

-(void)copiaBD{
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    
    NSString* bd = [documentsPath stringByAppendingPathComponent:@"mi bd"];
    BOOL existeArchivo = [[NSFileManager defaultManager] fileExistsAtPath:bd];
    if (existeArchivo) return;
    NSString *rutaResource = [[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:@"my bd"];
    NSFileManager *administradorArchivos=[NSFileManager defaultManager];
    NSError *error=nil;
    if (![administradorArchivos copyItemAtPath:rutaResource toPath:bd error:&error])
    {
        UIAlertView *mensajeError = [[UIAlertView alloc] initWithTitle:@"!Cuidado!" message:@"No pude copiar la bd" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [mensajeError show];
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)oprimir:(id)sender {
    [self.texto resignFirstResponder];
    [self.proxYBD insertarNombre:self.texto.text];
    [self.tabla reloadData];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return [[self.proxYBD nombres]count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = [[self.proxYBD nombres]objectAtIndex:indexPath.row];
    return  cell;
}

@end

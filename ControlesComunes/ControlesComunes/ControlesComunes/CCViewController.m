//
//  CCViewController.m
//  ControlesComunes
//
//  Created by Patlan on 05/03/14.
//  Copyright (c) 2014 Iniciales. All rights reserved.
//

#import "CCViewController.h"

@interface CCViewController ()

@end

@implementation CCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)procesarInformacion:(id)sender {
    NSUInteger longitud = [_texto.text length];
    NSMutableString * rtr=[NSMutableString stringWithCapacity:longitud];
    
    while (longitud > (NSUInteger)0) {
        unichar uch = [_texto.text characterAtIndex:--longitud];
        [rtr appendString:[NSString stringWithCharacters:&uch length:1]];
    }
    _etiqueta.text=rtr;
}
- (IBAction)terminaEdicion:(id)sender {
    [_texto resignFirstResponder];
}
@end

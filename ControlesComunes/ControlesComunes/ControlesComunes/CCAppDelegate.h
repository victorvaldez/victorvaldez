//
//  CCAppDelegate.h
//  ControlesComunes
//
//  Created by Patlan on 05/03/14.
//  Copyright (c) 2014 Iniciales. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

//
//  IDEAppDelegate.h
//  DemostracionIDE
//
//  Created by Patlan on 04/03/14.
//  Copyright (c) 2014 ITLM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IDEAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

//
//  main.m
//  Calculadorapp
//
//  Created by Patlan on 21/02/14.
//  Copyright (c) 2014 Patlan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

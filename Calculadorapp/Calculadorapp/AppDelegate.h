//
//  AppDelegate.h
//  Calculadorapp
//
//  Created by Patlan on 21/02/14.
//  Copyright (c) 2014 Patlan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

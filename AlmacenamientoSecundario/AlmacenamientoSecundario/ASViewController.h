//
//  ASViewController.h
//  AlmacenamientoSecundario
//
//  Created by Angel Gabriel on 11/03/14.
//  Copyright (c) 2014 Instituto Tecnológico de Los Mochis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *texto;
- (IBAction)terminoescribir:(id)sender;

@end

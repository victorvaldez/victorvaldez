//
//  NLViewController.h
//  ServicioJSON
//
//  Created by Patlan on 10/04/14.
//  Copyright (c) 2014 organizacion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *temperatura;
@property (weak, nonatomic) IBOutlet UILabel *presion;
@property (weak, nonatomic) IBOutlet UILabel *poblacion;

@end
